const mqtt = require("mqtt");
const SHA256 = require("crypto-js/sha256");
const CryptoJS = require("crypto-js");

// Configuration
const MQTT_SERVER = "mqtt://mqtt.octr.ee";
const FROM_USER = "karen";
const channel = process.argv[2];
const message = process.argv[3];
const SYM_KEY = "cornemuse";

const EC = require("elliptic").ec;
const ec = new EC("secp256k1");

// Création du client MQTT et connexion au broker
const client = mqtt.connect(MQTT_SERVER);

// Quand la connexion est établie
client.on("connect", () => {
  console.log(`Connecté à ${MQTT_SERVER}`);
  console.log(
    `Envoi du message '${message}' dans le cannal ${channel} sous le nom ${FROM_USER}`
  );

  // Préparation du payload
  const PRIVATE_KEY = "270007f26d9e54ead79fb9a76289e6aa4334d5116f1ab03e07e3452475b579d0"
  const cipherMessage = CryptoJS.AES.encrypt(message, SYM_KEY).toString();
  const checksum = SHA256(cipherMessage).toString();
  const keys = ec.keyFromPrivate(PRIVATE_KEY);
  const signature = keys.sign(checksum).toDER();

  const payload = JSON.stringify({
    message: cipherMessage,
    from: FROM_USER,
    checksum: checksum,
    password: "dondusang",
    signature: signature
  });

  // Publication du message au broker
  client.publish(channel, payload);

  console.log("Fermeture de la connexion");
  client.end();
});
