const mqtt = require("mqtt");
const SHA256 = require("crypto-js/sha256");
const CryptoJS = require("crypto-js");

// Configuration
const CHANNEL = "tim";
const MQTT_SERVER = "mqtt://mqtt.octr.ee";
// const PASSWORD = "dondusang"
const SYM_KEY = "cornemuse";
const PASSWORD_HASH = "78d342683ed59f31517bc12eebdd498eaec5fb8c717f7f0b496ea33ec2a50f22"

// Création du client MQTT et connexion au broker
const client = mqtt.connect(MQTT_SERVER);

// Quand la connexion est établie
client.on("connect", () => {
  console.log(`Connecté à ${MQTT_SERVER}`);

  client.subscribe(CHANNEL, err => {
    if (err) throw new Error(err);
    console.log(`En écoute des messages du canal '${CHANNEL}'`);
  });
});

// Quand on reçoit un message dans le channel
client.on("message", (channel, buffer) => {
  const payloadRaw = buffer.toString();
  const payload = JSON.parse(payloadRaw);
  const { message, from, checksum } = payload;

  const decryptMessage = CryptoJS.AES.decrypt(SYM_KEY).toString(
    CryptoJS.enc.Utf8
  );

  //Vérification du checksum
  const verifychecksum = SHA256(message).toString();
  if (verifychecksum !== checksum) {
    console.log(verifychecksum, checksum);
    console.log("Il y a un problème sur le lien" + from);
    return
  }

  if (password !== PASSWORD_HASH) {
    console.log('non autorisé');
    return
  }

  console.log(`Reçu de ${from}: ${decryptMessage} Intégration ok`)
});
