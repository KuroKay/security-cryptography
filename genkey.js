const EC = require("elliptic").ec;
const ec = new EC("secp256k1");

const keys = ec.genKeyPair();
console.log(`ma clé publique: ${keys.getPublic('hex')} `);
console.log(`ma clé privée: ${keys.getPrivate('hex')} `);

// 04214b2109949d01a5dbec62ef8d4d432888385cdca8ad53829c4cd38819d8757a012b901694e527dc6ed8d1f35d39c2ae3d3b5e1131b039659acd22ed24d672c3 
// 270007f26d9e54ead79fb9a76289e6aa4334d5116f1ab03e07e3452475b579d0