// Init
let str = "";
let letters = "abcdefghijklmnopqrstuvwxyz";
// use a regex
const splitLetters = letters.match(/.{1,2}/g);

// a variable for each letter of the word (4-letter word)
let letter01 = 0;
let letter02 = 0;
let letter03 = 0;
let letter04 = 0;

// Hash
const calculateHash = str => {
  let hash = str
    .split("")
    .map((c, i) => str.charCodeAt(i))
    .map(c => c + 2)
    .map(c => String.fromCharCode(c))
    .join("");
  return Buffer.from(hash).toString("base64");
};

// Test with : nope

while (calculateHash(str) != "eWd1ag==") {
  str =
    letters[letter01] +
    letters[letter02] +
    letters[letter03] +
    letters[letter04];
  letter04 += 1;
  //26 letters of the alphabet
  if (letter04 === 26) {
    letter03 += 1;
    letter04 = 0;
  }
  if (letter03 === 26) {
    letter02 += 1;
    letter03 = 0;
  }
  if (letter02 === 26) {
    letter01 += 1;
    letter02 = 0;
  }
  if (letter01 === 26) {
    console.log(str);
    break;
  }
}
console.log(str);

// ATTENTION -> work just with lowercase

// hash eWd1ag== : Mot trouvé = wesh
// hash bm9wZQ== : Mot trouvé = nope
